package com.thedroidboy.currencies

import com.thedroidboy.currencies.model.entities.Rate
import com.thedroidboy.currencies.model.entities.RatesEntity
import org.junit.Test

import org.junit.Assert.*
import org.junit.Before

class TestRateConversion: BaseTest() {

    lateinit var ratesEntity: RatesEntity

    @Before
    override fun setUp() {
        super.setUp()
        ratesEntity = getRateEntityFromJson()
    }

    @Test
    fun testConvertRateResponseToRateList() {
        val rateList = Rate.convertRateRawMapToRateList(ratesEntity.rates.toSortedMap())
        assertNotNull(rateList)
        //we add base to list as well
        assertEquals(ratesEntity.rates.size + 1, rateList.size)
    }

}

