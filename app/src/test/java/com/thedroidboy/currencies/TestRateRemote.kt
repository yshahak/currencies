package com.thedroidboy.currencies

import com.thedroidboy.currencies.model.entities.RatesEntity
import com.thedroidboy.currencies.model.repository.remote.RemoteStoreImpl
import org.junit.Test

import org.junit.Assert.*
import org.junit.Before

class TestRateRemote {

    lateinit var ratesEntity: RatesEntity

    @Before
    fun setUp() {
        ratesEntity = RemoteStoreImpl().getRateEntity("EUR")!!
    }

    @Test
    internal fun testRateService() {
        assertNotNull(ratesEntity.base)
        assertEquals("EUR",ratesEntity.base)
        assertNotNull(ratesEntity.date)
        assertEquals(10,ratesEntity.date.length)
        assertNotNull(ratesEntity.rates)
        assertTrue(ratesEntity.rates.isNotEmpty())
    }



}