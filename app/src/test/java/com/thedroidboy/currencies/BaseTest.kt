package com.thedroidboy.currencies

import com.google.gson.GsonBuilder
import com.thedroidboy.currencies.model.entities.RatesEntity
import org.junit.Before

abstract class BaseTest {

    private lateinit var rateAsStr: String

    @Suppress("RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
    @Before
    open fun setUp() {
        rateAsStr = this::class.java.classLoader.getResource("Rates.json")!!.readText()
    }

    fun getRateEntityFromJson(): RatesEntity = GsonBuilder().create().fromJson(rateAsStr, RatesEntity::class.java)
}