package com.thedroidboy.currencies

import com.thedroidboy.currencies.model.entities.RatesEntity
import com.thedroidboy.currencies.model.repository.RateRepository
import com.thedroidboy.currencies.model.repository.RateRepositoryImpl
import com.thedroidboy.currencies.model.repository.RateStore
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock


class TestRateRepository : BaseTest() {

    private val remoteStore = mock(RateStore::class.java)
    private lateinit var rateRepository: RateRepository
    private lateinit var ratesEntity: RatesEntity

    @Before
    override fun setUp() {
        super.setUp()
        //we don't want to hit the server so we use the RepoImpl just for convert the map
        ratesEntity = getRateEntityFromJson()
        `when`(remoteStore.getRateEntity("EUR")).thenReturn(ratesEntity)
        rateRepository = RateRepositoryImpl(remoteStore)
    }

    @Test
    internal fun testGetRateList() {
        val rateList = rateRepository.fetchRates()!!
        assertNotNull(rateList)
        assertEquals("EUR", rateList.first().name)
        assertEquals(1f, rateList.first().rateValue)
        for (rate in rateList) {
            assertTrue(rate.rateValue > 0)
        }
    }

    @Test
    fun testBaseFactorChange() {
        rateRepository.fetchRates()
        rateRepository.onBaseFactorChange(2.5f)
        val rateList = rateRepository.fetchRates()!!
        //first is the base and not included in map
        for (rate in rateList.subList(1, rateList.lastIndex)) {
            assertEquals(ratesEntity.rates[rate.name]!! * 2.5f, rate.rateValue)
        }
    }

    @Test
    fun testRowTapped() {
        val rateList = rateRepository.fetchRates()!!
        assertEquals("EUR", rateList.first().name)
        val tappedRate = rateList[10]
        rateRepository.onRowTapped(tappedRate)
        assertEquals(tappedRate.name, rateList.first().name)
        assertEquals(tappedRate.rateValue, rateList.first().rateValue)
        assertEquals("EUR", rateList[1].name)
    }
}