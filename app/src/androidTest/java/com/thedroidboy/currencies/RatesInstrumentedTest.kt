package com.thedroidboy.currencies


import android.view.View
import androidx.arch.core.executor.testing.CountingTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.NoMatchingViewException
import androidx.test.espresso.ViewAssertion
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.BoundedMatcher
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.rule.ActivityTestRule
import androidx.test.runner.AndroidJUnit4
import com.thedroidboy.currencies.model.entities.Rate
import com.thedroidboy.currencies.model.repository.RateRepositoryImpl
import com.thedroidboy.currencies.model.repository.RateStore
import com.thedroidboy.currencies.view.RatesFragment
import com.thedroidboy.currencies.viewmodel.RateViewModel
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.Mockito.doNothing
import org.mockito.MockitoAnnotations


@Suppress("DEPRECATION","unused", "RemoveRedundantQualifierName")
@RunWith(AndroidJUnit4::class)
class RatesInstrumentedTest {


    private val liveData = MutableLiveData<List<Rate>>()
    @get:Rule
    val activityRule: ActivityTestRule<MainTestActivity> = ActivityTestRule(MainTestActivity::class.java)
    @Rule
    @JvmField
    val countingTaskExecutorRule = CountingTaskExecutorRule()

    @Mock
    private lateinit var viewModelProvider: ViewModelProvider
    @Mock
    private lateinit var viewModel: RateViewModel
    @Mock
    private lateinit var rateRate: RateStore
    private lateinit var rateRepo : RateRepositoryImpl

    private val rateList = mutableListOf(
        Rate("EUR", R.string.currency_eur_name, R.mipmap.ic_eur_flag, 1f),
        Rate("USD", R.string.currency_usd_name, R.mipmap.ic_usd_flag, 1.3f),
        Rate("ILS", R.string.currency_ils_name, R.mipmap.ic_ils_flag, 4.2f)
    )

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        rateRepo = RateRepositoryImpl(rateRate)
        rateRepo.rateList = rateList
        `when`(viewModel.rateRepository).thenReturn(rateRepo)
        `when`(viewModel.rateListLiveData).thenReturn(liveData)
        `when`(viewModel.positionChanged).thenReturn(MutableLiveData())
        `when`(viewModel.errorLiveData).thenReturn(MutableLiveData())
        doNothing().`when`(viewModel).onViewResumed()
        doNothing().`when`(viewModel).onViewStop()
        `when`(viewModelProvider.get(RateViewModel::class.java)).thenReturn(viewModel)
        val ratesFragment = RatesFragment()
        ratesFragment.setViewModelProvider(viewModelProvider)
        activityRule.activity.setFragment(ratesFragment)
    }

    @Test
    fun testProgressIsLoadedFirst() {
        onView(withId(R.id.progress_circular)).check(matches(isCompletelyDisplayed()))
    }

    @Test
    fun testProgressIsGone() {
        liveData.postValue((rateList))
        onView(withId(R.id.progress_circular)).check(matches(ViewMatchers.withEffectiveVisibility(ViewMatchers.Visibility.GONE)))
    }

    @Test
    fun testRecyclerAdapterNullability() {
        onView(withId(R.id.recycler_view)).check(RecyclerViewAdapterNullAssertion(true))
        liveData.postValue((rateList))
        onView(withId(R.id.recycler_view)).check(RecyclerViewAdapterNullAssertion(false))
    }

    @Test
    fun testRecyclerCount() {
        liveData.postValue((emptyList()))
        onView(withId(R.id.recycler_view)).check(RecyclerViewItemCountAssertion(0))
        liveData.postValue((rateList))
        onView(withId(R.id.recycler_view)).check(RecyclerViewItemCountAssertion(rateList.size))
    }

    @Test
    fun testFirstItemIsCorrect() {
        liveData.postValue((rateList))
        onView(ViewMatchers.withId(R.id.recycler_view))
            .perform(RecyclerViewActions.scrollToPosition<RecyclerView.ViewHolder>(0))
            .check(matches(atPosition(0, hasDescendant(withText("EUR")))))
            .check(matches(atPosition(0, hasDescendant(withText("1.00")))))
    }

    @Test
    fun testSwipeElements() {
        liveData.postValue((rateList))
        onView(ViewMatchers.withId(R.id.recycler_view)).perform(
            RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(
                1, click()
            )
        )
        onView(ViewMatchers.withId(R.id.recycler_view))
            .check(matches(atPosition(0, hasDescendant(withText("USD")))))
            .check(matches(atPosition(0, hasDescendant(withText("1.30")))))
        onView(ViewMatchers.withId(R.id.recycler_view)).perform(
            RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(
                2, click()
            )
        )
        onView(ViewMatchers.withId(R.id.recycler_view))
            .check(matches(atPosition(0, hasDescendant(withText("ILS")))))
            .check(matches(atPosition(0, hasDescendant(withText("4.20")))))
    }

    @Test
    fun testChangeFactor() {
        liveData.postValue((rateList))
        onView(withId(R.id.rate_edit_text)).perform(clearText(), typeText("1.42"))
        //ugly, but useful. We want to wait for the callback to be called
        Thread.sleep(100)
        onView(ViewMatchers.withId(R.id.recycler_view))
            .check(matches(atPosition(0, hasDescendant(withText("EUR")))))
            .check(matches(atPosition(0, hasDescendant(withText("1.42")))))
        onView(ViewMatchers.withId(R.id.recycler_view))
            .check(matches(atPosition(1, hasDescendant(withText("USD")))))
            .check(matches(atPosition(1, hasDescendant(withText(String.format("%.4f", 1.42 * 1.3))))))

    }
}

class RecyclerViewItemCountAssertion(private val expectedCount: Int) : ViewAssertion {
    override fun check(view: View, noViewFoundException: NoMatchingViewException?) {
        if (noViewFoundException != null) {
            throw noViewFoundException
        }
        val recyclerView = view as RecyclerView
        val adapter = recyclerView.adapter
        assertNotNull(adapter)
        assertEquals(expectedCount, adapter!!.itemCount)
    }
}

class RecyclerViewAdapterNullAssertion(private val isNull: Boolean) : ViewAssertion {
    override fun check(view: View, noViewFoundException: NoMatchingViewException?) {
        if (noViewFoundException != null) {
            throw noViewFoundException
        }
        val recyclerView = view as RecyclerView
        val adapter = recyclerView.adapter
        takeIf { isNull }?.apply {
            assertNull(adapter)
        } ?: run {
            assertNotNull(adapter)
        }
    }
}

fun atPosition(position: Int, itemMatcher: Matcher<View>): Matcher<View> {
    return object : BoundedMatcher<View, RecyclerView>(RecyclerView::class.java) {

        override fun describeTo(description: Description) {
            description.appendText("has item at position $position: ")
            itemMatcher.describeTo(description)
        }

        override fun matchesSafely(view: RecyclerView): Boolean {
            val viewHolder = view.findViewHolderForAdapterPosition(position) ?: // has no item on such position
            return false
            return itemMatcher.matches(viewHolder.itemView)
        }
    }
}
