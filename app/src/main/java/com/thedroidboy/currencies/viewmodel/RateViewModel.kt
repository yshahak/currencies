package com.thedroidboy.currencies.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.thedroidboy.currencies.model.entities.Rate
import com.thedroidboy.currencies.model.repository.RateRepositoryImpl
import com.thedroidboy.currencies.model.repository.remote.RemoteStoreImpl
import kotlinx.coroutines.*

//this class and it members should not be open, but for some reason the kotlin plugin 'kotlin-allopen" didn't work in this project
@Suppress("SameParameterValue")
open class RateViewModel : ViewModel() {

    open val rateRepository: RateRepositoryImpl by lazy { RateRepositoryImpl(RemoteStoreImpl()) }
    //expose LiveData with updated rates
    open val rateListLiveData: MutableLiveData<List<Rate>> = MutableLiveData()
    //expose LiveData with change in order of items. The int is the old index of the tapped item
    open val positionChanged: MutableLiveData<Int> = MutableLiveData()
    //expose LiveData to notify about error
    open val errorLiveData: MutableLiveData<String> = MutableLiveData()
    //flag to indicate that should skip next iteration for consistent data
    private var skipNextIteration = false
    private lateinit var sampleJob: Job

    open fun onViewResumed(){
        startSampleJob()
    }

    open fun onViewStop(){
        stopSampleJob()
    }

    /**
     * sample Rate Repository every given interval
     * @param repeatDurationMillis interval
     */
    private fun startSampleJob(repeatDurationMillis: Long = 1000) {
        var delay = repeatDurationMillis
        //viewModelScope manage canceling the job when it life cycle onCleared() is called
        sampleJob = viewModelScope.launch {
            repeat(Int.MAX_VALUE) {
                if (skipNextIteration) {
                    skipNextIteration = false
                } else {
                    val rateList = sampleRateRepeatedly()
                    rateList?.let {
                        delay = repeatDurationMillis
                        //update ui with updated list
                        rateListLiveData.postValue(rateList)
                    } ?: apply {
                        errorLiveData.postValue("Cannot fetch rates")
                        //we will increase the delay for next execution because of the error
                        delay += delay
                    }
                }
                delay(delay)
            }
        }
    }

    /**
     * stop sampling rates
     */
    private fun stopSampleJob(){
        sampleJob.cancel()
    }

    /**
     * sample RatesRepository in Coroutine and return List<Rate>
     */
    private suspend fun sampleRateRepeatedly(): List<Rate>? = withContext(Dispatchers.Default) {
        return@withContext rateRepository.fetchRates()
    }

    /**
     * onClick callback from databinding layout file rate_row.xml wired to here
     */
    fun onRowClicked(rate: Rate) {
        val position = rateListLiveData.value?.indexOf(rate)
        //skip next iteration to avoid racing issues
        skipNextIteration = true
        rateRepository.onRowTapped(rate)
        positionChanged.value = position
    }

    /**
     * callback from databinding layout file rate_row_header.xml EditText wired to here
     * @param factor new value from form
     */

    fun onBaseFactorChange(factor: Float) {
        if (rateRepository.onBaseFactorChange(factor)){
            rateListLiveData.value = rateListLiveData.value
        }
    }

}