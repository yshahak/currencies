package com.thedroidboy.currencies.view

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.thedroidboy.currencies.R


class MainActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        supportFragmentManager.beginTransaction()
            .add(R.id.fragment_container, RatesFragment(), "rate_fragment")
            .commit()
    }

}

