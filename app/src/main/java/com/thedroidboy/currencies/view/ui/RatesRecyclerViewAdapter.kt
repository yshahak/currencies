package com.thedroidboy.currencies.view.ui

import android.text.Editable
import android.text.TextWatcher
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.thedroidboy.currencies.BR
import com.thedroidboy.currencies.databinding.RateRowBinding
import com.thedroidboy.currencies.databinding.RateRowHeaderBinding
import com.thedroidboy.currencies.model.entities.Rate
import com.thedroidboy.currencies.viewmodel.RateViewModel


class RatesRecyclerViewAdapter(
    var rates: List<Rate>, private val eventListener: RateViewModel
) : RecyclerView.Adapter<ItemViewHolder>(), TextView.OnEditorActionListener {


    companion object {
        const val VIEW_TYPE_HEADER = 0
        const val VIEW_TYPE_ROW = 1
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val itemForBinding = if (viewType == VIEW_TYPE_HEADER) {
            RateRowHeaderBinding.inflate(layoutInflater, parent, false).apply {
                //bind text watcher to the header
                this.setVariable(BR.textWatcher, this@RatesRecyclerViewAdapter)
                this.rateEditText.setOnEditorActionListener(this@RatesRecyclerViewAdapter)
            }
        } else {
            RateRowBinding.inflate(layoutInflater, parent, false).apply {
                //click is enabled for not header rows
                this.setVariable(BR.vm, eventListener)
            }
        }
        return ItemViewHolder(itemForBinding)
    }

    override fun getItemViewType(position: Int): Int {
        return if (position == 0) {
            VIEW_TYPE_HEADER
        } else {
            VIEW_TYPE_ROW
        }
    }

    override fun getItemCount() = rates.size

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.bind(rates[position])
    }

    fun getRateTextWatcher(): TextWatcher {
        return object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
            }

            override fun afterTextChanged(s: Editable) {
                if (s.isNotEmpty()) {
                    eventListener.onBaseFactorChange(s.toString().toFloat())
                }
            }
        }
    }

    override fun onEditorAction(v: TextView?, actionId: Int, event: KeyEvent?): Boolean {
        if (actionId == EditorInfo.IME_ACTION_DONE) {
            v?.apply {
                this.clearFocus()
            }
        }
        return false
    }


}

class ItemViewHolder(private val binding: ViewDataBinding) : RecyclerView.ViewHolder(binding.root) {

    fun bind(item: Rate) {
        binding.setVariable(BR.rate, item)
        binding.executePendingBindings()
    }
}


