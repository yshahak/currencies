package com.thedroidboy.currencies.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.thedroidboy.currencies.R
import com.thedroidboy.currencies.databinding.RateFragmentBinding
import com.thedroidboy.currencies.model.entities.Rate
import com.thedroidboy.currencies.view.ui.RatesRecyclerViewAdapter
import com.thedroidboy.currencies.viewmodel.RateViewModel
import kotlinx.android.synthetic.main.rate_fragment.view.*
import androidx.lifecycle.ViewModelProvider
import androidx.annotation.VisibleForTesting







class RatesFragment: Fragment() {

    lateinit var ratesViewModel: RateViewModel
    private var viewModelProvider: ViewModelProvider? = null


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val binding : RateFragmentBinding = DataBindingUtil.inflate(inflater, R.layout.rate_fragment, container, false)
        ratesViewModel = getViewModelProvider(this).get(RateViewModel::class.java)
        binding.isLoading = true
        val recyclerView = binding.root.recycler_view
        recyclerView.layoutManager = LinearLayoutManager(activity)
        ratesViewModel.rateListLiveData.observe(this, Observer<List<Rate>> { rates ->
            // update UI
            recyclerView.adapter?.apply {
                (this as RatesRecyclerViewAdapter).rates = rates
                //we don't want to update header
                notifyItemRangeChanged(1, rates.size - 1, true)
            } ?: let {
                //initialize the adapter
                recyclerView.adapter = RatesRecyclerViewAdapter(rates, ratesViewModel)
                binding.isLoading = false
            }
        })
        ratesViewModel.positionChanged.observe(this, Observer<Int> { position ->
            // update UI
            recyclerView.adapter?.apply {
                this.notifyItemMoved(position, 0)
                recyclerView.scrollToPosition(0)
                this.notifyItemChanged(0)
                this.notifyItemChanged(1)
                this.notifyItemChanged(position)
            }
        })
        ratesViewModel.errorLiveData.observe(this, Observer<String> { error ->
            // update UI
            Toast.makeText(this@RatesFragment.activity, error, Toast.LENGTH_SHORT).show()
        })
        return binding.root
    }

    override fun onResume() {
        super.onResume()
        ratesViewModel.onViewResumed()
    }

    override fun onStop() {
        super.onStop()
        ratesViewModel.onViewStop()
    }

    @VisibleForTesting
    fun setViewModelProvider(viewModelProvider: ViewModelProvider) {
        this.viewModelProvider = viewModelProvider
    }

    private fun getViewModelProvider(fragment: Fragment): ViewModelProvider {
        takeIf { viewModelProvider == null }?.apply {
            viewModelProvider = ViewModelProviders.of(fragment)
        }
        return viewModelProvider as ViewModelProvider
    }
}