package com.thedroidboy.currencies.model.entities

/**
 * class represent response object from remote
 */
data class RatesEntity(var base: String, val date: String, val rates: MutableMap<String, Float>)