package com.thedroidboy.currencies.model.repository

import androidx.annotation.WorkerThread
import com.thedroidboy.currencies.model.entities.Rate

class RateRepositoryImpl(private val rateStore: RateStore) : RateRepository {

    private var factor = 1f
    private var base: String = "EUR"
    lateinit var rateList: MutableList<Rate>

    @WorkerThread
    override fun fetchRates(): List<Rate>? {
        val rates = rateStore.getRateEntity(base)
        rates?.let {
            //check if first run and rateEntity not assigned yet
            if (!::rateList.isInitialized) {
                //support switch base easily by adding the base itself to the rate map
                //this List will be created once and all changes will be applied to same copy
                rateList = Rate.convertRateRawMapToRateList(rates.rates)
            } else {
                //update values to all Rate object
                rateList.filter { it.name != base }.forEach {
                    rates.rates[it.name]?.apply {
                        it.rateValue = this * factor
                    }

                }
            }
            return rateList
        } ?: return null

    }

    override fun onBaseFactorChange(newFactor: Float): Boolean {
        //avoiding false positive duo to formatting differences
        if (factor >= 0 && String.format("%.2f", factor) != String.format("%.2f", newFactor)) {
            val diff = newFactor / factor
            factor = newFactor
            rateList.forEach {
                it.rateValue *= diff
            }
            //change in factor need to be reflected on all list values in ui
            return true
        }
        return false
    }

    override fun onRowTapped(tappedRate: Rate) {
        base = tappedRate.name
        factor = tappedRate.rateValue
        rateList.remove(tappedRate)
        rateList.add(0, tappedRate)
    }

}

/**
 * responsible to bring updated data to the caller. Can be from remote or local db
 */
interface RateRepository {

    /**
     * get fresh copy of rates from repository
     * @return List<Rate>
     */
    fun fetchRates(): List<Rate>?

    /**
     * handle logic for rate tap
     * @param tappedRate which rate was tapped
     */
    fun onRowTapped(tappedRate: Rate)

    /**
     * handle change factor
     * @param newFactor for this base
     * @return <b>true</b> if need to update ui
     */
    fun onBaseFactorChange(newFactor: Float): Boolean
}
