package com.thedroidboy.currencies.model.repository

import com.thedroidboy.currencies.model.entities.RatesEntity

/**
 * responsible to return RatesEntity from store
 */
interface RateStore {
    /**
     * get raw Rates data
     * @param base to calculate rates against
     */
    fun getRateEntity(base: String): RatesEntity?
}

