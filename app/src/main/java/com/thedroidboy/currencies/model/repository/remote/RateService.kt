package com.thedroidboy.currencies.model.repository.remote

import androidx.annotation.WorkerThread
import com.thedroidboy.currencies.BuildConfig
import com.thedroidboy.currencies.model.entities.RatesEntity
import com.thedroidboy.currencies.model.repository.RateStore
import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit


/**
 * Remote implementation for RateStore
 */
class RemoteStoreImpl: RateStore {

    @WorkerThread
    override fun getRateEntity(base: String): RatesEntity? {
        try {
            val response = rateService.getRateMap(base).execute()
            takeIf { response.isSuccessful }?.apply {
                response.body()?.let {
                    return it
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return null
    }

    companion object {

        private val okHttpClient: OkHttpClient = OkHttpClient.Builder()
            .connectTimeout(3, TimeUnit.SECONDS)
            .build()
        val rateService: RateService = Retrofit.Builder()
            .client(okHttpClient)
            .baseUrl(BuildConfig.SERVER_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build().create(RateService::class.java)
    }

    /**
     * Retrofit service to communicate with server
     */
    interface RateService {
        @GET("latest")
        fun getRateMap(@Query("base") base: String): Call<RatesEntity>
    }
}

