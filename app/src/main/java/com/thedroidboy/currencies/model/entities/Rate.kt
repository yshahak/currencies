package com.thedroidboy.currencies.model.entities

import com.thedroidboy.currencies.R


@Suppress("MapGetWithNotNullAssertionOperator")
data class Rate(val name: String, val displayNameRes: Int, val iconRes: Int, var rateValue: Float) {

    companion object {
        /**
         * converting Map<String, Float> to MutableList<Rate>
         * taking care to add base to map as well
         */
        fun convertRateRawMapToRateList(rateMap: Map<String, Float>, base: String = "EUR"): MutableList<Rate> {
            return rateMap.map {
                Rate(
                    it.key,
                    labelToDisplayNameMap.getOrElse(it.key) { R.string.app_name },
                    labelToIconMap.getOrElse(it.key) { R.mipmap.ic_launcher },
                    it.value
                )
            }.toMutableList().apply {
                this.add(0, Rate(base, labelToDisplayNameMap[base]!!, labelToIconMap[base]!!, 1f))
            }
        }
    }
}

val labelToDisplayNameMap = mapOf(
    "AUD" to R.string.currency_aud_name,
    "BGN" to R.string.currency_bgn_name,
    "BRL" to R.string.currency_brl_name,
    "CAD" to R.string.currency_cad_name,
    "CHF" to R.string.currency_chf_name,
    "CNY" to R.string.currency_cny_name,
    "CZK" to R.string.currency_czk_name,
    "DKK" to R.string.currency_dkk_name,
    "EUR" to R.string.currency_eur_name,
    "GBP" to R.string.currency_gbp_name,
    "HKD" to R.string.currency_hkd_name,
    "HRK" to R.string.currency_hrk_name,
    "HUF" to R.string.currency_huf_name,
    "IDR" to R.string.currency_idr_name,
    "ILS" to R.string.currency_ils_name,
    "INR" to R.string.currency_inr_name,
    "ISK" to R.string.currency_isk_name,
    "JPY" to R.string.currency_jpy_name,
    "KRW" to R.string.currency_krw_name,
    "MXN" to R.string.currency_mxn_name,
    "MYR" to R.string.currency_myr_name,
    "NOK" to R.string.currency_nok_name,
    "NZD" to R.string.currency_nzd_name,
    "PHP" to R.string.currency_php_name,
    "PLN" to R.string.currency_pln_name,
    "RON" to R.string.currency_ron_name,
    "RUB" to R.string.currency_rub_name,
    "SEK" to R.string.currency_sek_name,
    "SGD" to R.string.currency_sgd_name,
    "THB" to R.string.currency_thb_name,
    "TRY" to R.string.currency_try_name,
    "USD" to R.string.currency_usd_name,
    "ZAR" to R.string.currency_zar_name
)
val labelToIconMap = mapOf(
    "AUD" to R.mipmap.ic_aud_flag,
    "BGN" to R.mipmap.ic_bgn_flag,
    "BRL" to R.mipmap.ic_brl_flag,
    "CAD" to R.mipmap.ic_cad_flag,
    "CHF" to R.mipmap.ic_chf_flag,
    "CNY" to R.mipmap.ic_cny_flag,
    "CZK" to R.mipmap.ic_czk_flag,
    "DKK" to R.mipmap.ic_dkk_flag,
    "EUR" to R.mipmap.ic_eur_flag,
    "GBP" to R.mipmap.ic_gbp_flag,
    "HKD" to R.mipmap.ic_hkd_flag,
    "HRK" to R.mipmap.ic_hrk_flag,
    "HUF" to R.mipmap.ic_huf_flag,
    "IDR" to R.mipmap.ic_idr_flag,
    "ILS" to R.mipmap.ic_ils_flag,
    "INR" to R.mipmap.ic_inr_flag,
    "ISK" to R.mipmap.ic_isk_flag,
    "JPY" to R.mipmap.ic_jpy_flag,
    "KRW" to R.mipmap.ic_krw_flag,
    "MXN" to R.mipmap.ic_mxn_flag,
    "MYR" to R.mipmap.ic_myr_flag,
    "NOK" to R.mipmap.ic_nok_flag,
    "NZD" to R.mipmap.ic_nzd_flag,
    "PHP" to R.mipmap.ic_php_flag,
    "PLN" to R.mipmap.ic_pln_flag,
    "RON" to R.mipmap.ic_ron_flag,
    "RUB" to R.mipmap.ic_rub_flag,
    "SEK" to R.mipmap.ic_sek_flag,
    "SGD" to R.mipmap.ic_sgd_flag,
    "THB" to R.mipmap.ic_thb_flag,
    "TRY" to R.mipmap.ic_try_flag,
    "USD" to R.mipmap.ic_usd_flag,
    "ZAR" to R.mipmap.ic_zar_flag
)

