Project for Revolut Android test
============================================

## Architecture: MVVM with Databinding

In this implementation, the ViewModel is responsible to persist UI LifeCycle, to notify ui for
date changes and to get callbacks from UI user actions.
The business logic managed by RateRepository class

The data is not persisted, but it can be easily implemented by a solid implementation of RateStore

